" F KEY MAPPINGS
" F1 = Toggle current fold
" F2 = Open all folds of current
" F3 = Close all folds in doc
" F4 = Open all folds in doc
" F5 = Next Buffer
" F6 = 
" F7 =
" F8 = Auto indent current block
" F9 = Toggle paste mode (doesn't autoindent)
" F10 = Resize split to 80 characters wide
" F11 = Split, and wrap file across two windows
" F12 = Toggle NERDTree

" Other key mappings
" Space = Leader
" Y = Yank until end of line
" v = expand region
" / = Search
" jk = ESC
" Leader c = remove cursor lines
" Leader w = write all
" Leader q = quit all


set nocompatible              " be iMproved, required
filetype indent plugin on                  " required
syntax on
set hidden "Allows switching from an unsaved buffer
set tabstop=2 " Set tab to two spaces
set shiftwidth=2
set expandtab " Tabs are spaces
set autoindent 
set smartindent
set number "Line Numbering
set backspace=indent,eol,start	" Backspace key is will wrap around lines
set ignorecase " Use case insensitive search, except when using capital letters
set smartcase
set mouse=a "Enable Mouse!
set wildmenu " Bottom compleation menu
set linespace=0 " Do not put a pixel between lines
set autoread " Automatically read when the file is externally changed
set cursorline
set cursorcolumn
set lazyredraw
set noeb vb t_vb=
"Backups and Undo
set showmatch " Show matching brackets
set mat=2 " Blink on match

"Colors
color peachpuff
:hi Folded ctermbg=232 "Fold line color
:hi Visual ctermbg=238 "Visual color
:hi LineNr ctermfg=240 "Line number color
:hi CursorLine   cterm=NONE ctermbg=233 
:hi CursorColumn cterm=NONE ctermbg=233  
:hi Search cterm=NONE ctermfg=grey ctermbg=blue
:hi MatchParen cterm=none ctermbg=green ctermfg=blue
":hi YcmErrorLine ctermbg=232
":hi SyntasticError cterm=none ctermbg=52
":hi SyntasticWarning cterm=none ctermbg=57




" Vundle (Plugin Manager)
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'gmarik/Vundle.vim'

"additional syntax highlighting for C++11/14
Plugin 'octol/vim-cpp-enhanced-highlight'
Bundle 'scrooloose/nerdtree'
Plugin 'terryma/vim-expand-region'
Plugin 'bling/vim-airline'
Plugin 'Lokaltog/vim-easymotion'
"Plugin 'scrooloose/syntastic'
"Plugin 'tpope/vim-fugitive'
"Plugin 'Valloric/YouCompleteMe'
"Plugin 'rdnetto/YCM-Generator'
" All of your Plugins must be added before the following line
call vundle#end()            " required

"Alt+Arrow Window Navigation
nmap <silent> <A-Up> :wincmd k<CR>
nmap <silent> <A-Down> :wincmd j<CR>
nmap <silent> <A-Left> :wincmd h<CR>
nmap <silent> <A-Right> :wincmd l<CR>


"Open NERDTree automatically, close automatically if last window
let NERDChristmasTree = 1
let NERDTreeMinimalUI = 1
let NERDTreeDirArrows = 0
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") && b:NERDTreeType == "primary") | q | endif


"Bottom toolbar
let g:airline#extensions#tabline#enabled = 1
"let g:airline_powerline_fonts=1
set laststatus=2
"let g:airline_theme='powerlineish'

"YouCompleteMe Options
"let g:ycm_auto_trigger = 1
"highlight Pmenu ctermfg=white ctermbg=232
"let g:ycm_confirm_extra_conf = 0
"let g:ycm_min_num_of_chars_for_completion = 2
"let g:ycm_global_ycm_extra_conf="~/.ycm_extra_conf.py"
"let g:ycm_autoclose_preview_window_after_completion = 1
"let g:ycm_autoclose_preview_window_after_insertion = 1
"let g:ycm_key_list_select_completion = ['<TAB>', '<Down>', '<Enter>']
"let g:ycm_filetype_blacklist = {
"      \ 'notes' : 1,
"      \ 'markdown' : 1,
"      \ 'text' : 1,
"      \ 'tex' : 1,
"      \}

":autocmd BufWritePost * execute ':silent ! if git rev-parse --git-dir > /dev/null 2>&1 ; then git add % ; git commit -m "Auto-commit: saved %"; fi > /dev/null 2>&1'

"Expand selection region (press v multiple times)
vmap v <Plug>(expand_region_expand)
vmap <C-v> <Plug>(expand_region_shrink)

"Easy Motion
map  / <Plug>(easymotion-sn)
omap / <Plug>(easymotion-tn)
map  n <Plug>(easymotion-next)
map  N <Plug>(easymotion-prev)

"F1 - F4 = Folding
set foldenable
set foldmethod=syntax
set foldlevelstart=99
inoremap <F1> <C-O>za
nnoremap <F1> za
onoremap <F1> <C-C>za
vnoremap <F1> zf
map <F2> zO
map <F3> zM
map <F4> zR

" F5 - Buffers
map <F5> <ESC>:bn<RETURN>

" F6 - Force YCM check
"nnoremap <F6> :YcmForceCompileAndDiagnostics<CR>

" F8 = Auto indent this block
map <F8> <ESC>=a{<RETURN>

"F9 - F12 = Misc
set pastetoggle=<F9>
map <F10> :vertical resize 80 <CR>
noremap <silent> <F11> ggzR:<C-u>let @z=&so<CR>:set so=0 noscb<CR>:set columns=240<CR>:bo vs<CR>zRLjzt:setl scb<CR><C-w>p:setl scb<CR>:let &so=@z<CR>
map <F12> :NERDTreeToggle<CR>
