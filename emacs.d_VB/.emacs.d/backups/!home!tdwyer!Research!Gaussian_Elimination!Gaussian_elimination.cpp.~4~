#ifndef SIZE
#define SIZE 4096
#define _TYPE_ float
#define TYPE "float" 
#endif

#include <stdio.h>
#include <stdlib.h>
#include <math.h> 
#include <chrono>
#include <string>
#include <mpi.h>


using namespace std;
void GE();
void swap_row(int r1, int r2);

MPI_Status status;
int hostID = 0;
int numSlaves = 1;
int curSlave = 1;
int host_to_slave = 100;
int slave_to_host = 200;
int chunkSize = SIZE;

int rowsPerProc = 1;
/**/
_TYPE_ a[SIZE*SIZE];
_TYPE_ b[SIZE];
_TYPE_ x[SIZE];

/**
  _TYPE_ a[] = {
    1.00, 0.00, 0.00,  0.00,  0.00, 0.00,
    1.00, 0.63, 0.39,  0.25,  0.16, 0.10,
    1.00, 1.26, 1.58,  1.98,  2.49, 3.13,
    1.00, 1.88, 3.55,  6.70, 12.62, 23.80,
    1.00, 2.51, 6.32, 15.88, 39.90, 100.28,
    1.00, 3.14, 9.87, 31.01, 97.41, 306.02
  };
  _TYPE_ b[] = { -0.01, 0.61, 0.91, 0.99, 0.60, 0.02 };
  _TYPE_ x[SIZE];
**/

_TYPE_ *A(int x, int y) 
{
  int col = x * SIZE;
  int row = y;
  return a + col + row;
}

void verify() 
{
  int aSum = 0;
  int bSum = 0;
  int xSum = 0;
  for (int i=0;i<SIZE;i++) {
    for (int j=0;j<SIZE;j++) {
      aSum += *A(i,j);
    }
    bSum += b[i];
    xSum += x[i];
  }
  printf("aSum: %d \t bSum: %d \t xSum: %d\n",aSum,bSum,xSum);

}

void print() 
{
  printf("Input A[*] | B[*]:\n");
  for (int i=0;i<SIZE;i++) {
    for (int j=0;j<SIZE;j++) {
      printf("%2.2f \t",*A(i,j));
    }
    printf(" | \t %2.2f\n",b[i]);
  }

  printf("Result x[*]:\n");
  for (int i=0;i<SIZE;i++) {
    printf("%2.2f \t",x[i]);
  }
  printf("\n\n");
}

//Serial
void findMax_and_swap(int curLoc) 
{
  //Find the maximum value starting at curLoc
  int max_row = curLoc;
  int n = SIZE;
  _TYPE_ max = *A(curLoc,curLoc); // Get a[dia,dia]
  for (int row = curLoc + 1; row < n; row++) {
    _TYPE_ tmp = *A(row,curLoc);
    if (tmp > max)
      max_row = row, max = tmp;
  }
  
  //Set up variable names (r1,r2 used for simplicity)
  int r1 = curLoc; 
  int r2 = max_row;
  _TYPE_ *p1, *p2;
  _TYPE_ tmp;

  //Check if rows are equal, and swap if not
  if (r1 == r2) return;
  for (int i = 0; i < n; i++) {
    p1 = A(r1,i); //Get a[r1,i]
    p2 = A(r2,i); //Get a[r2,i]
    tmp = *p1, *p1 = *p2, *p2 = tmp; // Swap two items
  }
  tmp = b[r1], b[r1] = b[r2], b[r2] = tmp; //Swap b matrix values too
}

//This only reads matrix A, can distribute rows over cores
void solve() 
{
  int n = SIZE;
  for (int row = n - 1; row >= 0; row--) {  //Iterate from bottom up
    _TYPE_ tmp = b[row];   
    for (int j = n - 1; j > row; j--)       //Iterate from cur loc up
      tmp -= x[j] * *A(row, j);     //Apply 
    x[row] = tmp / *A(row, row);    //
  }
}

//Original
void reduce_original(int curLoc) 
{
  int n = SIZE;
  for (int row = curLoc + 1; row < n; row++) {
    _TYPE_ tmp = *A(row, curLoc) / *A(curLoc, curLoc);      //Get scaling value
    for (int col = curLoc+1; col < n; col++) {              //Iterate through row
      *A(row, col) -= tmp * *A(curLoc, col);                //Scale row by scaling value
    }
    *A(row, curLoc) = 0;                                    //Zero out diagonal
    b[row] -= tmp * b[curLoc];                              //Scale b value
  }
}

void reduce_original2(int curLoc) 
{

  int n = SIZE;
  for (int row = curLoc+1; row < n; row++) {
    *A(row,curLoc) = *A(row, curLoc) / *A(curLoc, curLoc);      //Get scaling value
    b[row] -= *A(row,curLoc) * b[curLoc];                              //Scale b value
  }

  for (int row = curLoc+1; row < n; row++) {  
    for (int col = curLoc+1; col < n; col++) {              //Iterate through row
      *A(row, col) -= *A(row,curLoc) * *A(curLoc, col);     //Scale row by scaling value
    }
    *A(row, curLoc) = 0;                                    //Zero out diagonal
  }
}

//Function to do row reduction on a single chunk
//Assumes data is already copied into tempRow
_TYPE_ *doChunk(_TYPE_ *chunk, _TYPE_ *tempScale, int curLoc) 
{
    for (int col = 1; col < SIZE-curLoc; col++) {
      //Perform reduction
      chunk[col] -= chunk[0] * tempScale[col];
    }
    chunk[0] = 0;
    return chunk;
}


void receiveScale(int curLoc, _TYPE_ &tempScale)
{
  MPI_Recv(&tempScale, SIZE-curLoc, MPI_FLOAT, hostID, host_to_slave, MPI_COMM_WORLD, &status);
}

//Function for slave to recieve a chunk from the host
bool receiveChunk(int id, _TYPE_ &tempRow, _TYPE_ &tempScale) 
{  
  int curLoc, row;
  
  //Receive starting location (-1 is the exit code)
  MPI_Recv(&curLoc, 1, MPI_INT, hostID, host_to_slave, MPI_COMM_WORLD, &status);

  //Check for exit, or scale condition
  if (curLoc == -1) { //Exit condition
    return false;
  } else if (curLoc < -1) { //Scale refresh check (curLoc is offset by 10)
    receiveScale((curLoc * -1) - 10, tempScale);
    return true;
  }

  //Receive necessary data
  MPI_Recv(&row, 1, MPI_INT, hostID, host_to_slave, MPI_COMM_WORLD, &status);
  MPI_Recv(&tempRow,   SIZE-curLoc, MPI_FLOAT, hostID, host_to_slave, MPI_COMM_WORLD, &status);

  //Process data
  //  printf("Slave %d, doing work on row %d from curLoc %d\n",id, row, curLoc);
  doChunk(&tempRow,&tempScale,curLoc);

  //Send back result to host
  int data[2] = {curLoc, row};
  MPI_Send(data,  2,           MPI_INT,   hostID, slave_to_host, MPI_COMM_WORLD);
  //  MPI_Send(&row,  1,           MPI_INT,   hostID, slave_to_host, MPI_COMM_WORLD);
  MPI_Send(&tempRow, SIZE-curLoc, MPI_FLOAT, hostID, slave_to_host, MPI_COMM_WORLD);
  return true;
}

//Function for host to receive data from slave
//Saves data directly into global data structure (a)
int receiveFinishedChunk() 
{
  int data[2];
  MPI_Recv(data,          2,         MPI_INT,   MPI_ANY_SOURCE,    slave_to_host, MPI_COMM_WORLD, &status);
  int curLoc = data[0];
  int row = data[1];
  //  MPI_Recv(&row,	     1,		MPI_INT, status.MPI_SOURCE, slave_to_host, MPI_COMM_WORLD, &status);
  MPI_Recv(A(row,curLoc), SIZE-curLoc,  MPI_FLOAT, status.MPI_SOURCE, slave_to_host, MPI_COMM_WORLD, &status);
  //  printf("Host received compleation from slave %d, of row %d, on curLoc of %d\n",status.MPI_SOURCE,row, curLoc);
  return curLoc;
}


void sendScale(int slave, int curLoc)
{
  int curLoc_offset = (curLoc + 10) * -1;
  MPI_Send(&curLoc_offset, 1, MPI_INT, slave, host_to_slave, MPI_COMM_WORLD);
  MPI_Send(&a[curLoc*SIZE+curLoc], SIZE-curLoc, MPI_FLOAT,  slave, host_to_slave, MPI_COMM_WORLD);
}

//Function for the host to send a (partial) row of data to slave
//Send requires start location, the data, and the scaling factor
void sendChunk(int slave, int row, int curLoc) 
{  
  //Send necessary data
  MPI_Send(&curLoc,   1,           MPI_INT,		    slave, host_to_slave, MPI_COMM_WORLD);
  MPI_Send(&row,      1,           MPI_INT,		    slave, host_to_slave, MPI_COMM_WORLD);
  MPI_Send(&a[row*SIZE+curLoc],    SIZE-curLoc, MPI_FLOAT,  slave, host_to_slave, MPI_COMM_WORLD);
}

//Row reduction / Forward elimination
void reduce(int curLoc) 
{
  //Complex dependencies and O(N), leave serial
  for (int row = curLoc + 1; row < SIZE; row++) {
    *A(row,curLoc) = *A(row, curLoc) / *A(curLoc, curLoc);    //Get scaling value
    b[row] -= *A(row,curLoc) * b[curLoc];                       //Scale b value
  }

  for (int slave = 1; slave<numSlaves; slave++) {
    sendScale(slave,curLoc);
  }
  
  //Distribute work to slaves
  int curSlave = 1;
  int msgsSent=0;
  for (int row = curLoc+1; row < SIZE; row++) {
    //    printf("Host sending work to %d, row: %d, curLoc: %d\n",curSlave,row,curLoc);
    sendChunk(curSlave, row, curLoc);
    curSlave++;
    msgsSent++;
    if (curSlave >= numSlaves) curSlave = 1;
  }
      
  //Wait on all slaves to send you the results back
  //Assume all slaves are used ie. numSlaves < SIZE
  for (int msg=0; msg<msgsSent; msg++) 
    receiveFinishedChunk();
 
}

void GE(int flag)
{
  for (int curLoc = 0; curLoc < SIZE; curLoc++) {
    //Find the maximum row (start at curLoc), then swap those rows
    //Complexity O(2n)
    findMax_and_swap(curLoc);
    
    //Reduce this row
    //Complexity O(n^2)
    if (flag == 1) //Do all the work myself
      reduce_original(curLoc); 
    else          //Distribute work via MPI
      reduce(curLoc);

  }

  //Solve reduced matrix
  //Complexity O(~n^1.5)
  solve();
}

int
main(int argc, char **argv) {

  //Initialize MPI specifics
  MPI::Init (argc, argv);
  int id = MPI::COMM_WORLD.Get_rank();
  numSlaves = MPI::COMM_WORLD.Get_size();
  int np = ceil(SIZE/numSlaves);
  //  rowsPerProc = SIZE / (numSlaves - 1);
  chunkSize = SIZE;

  //Host code
  if (id == 0) 
  {
    printf("Gaussian Elimination initalized. Processes: %d, chunkSize: %d, ID: %d\n",numSlaves,chunkSize,id);
    //Initialize matrix with random values
    //srand (time(NULL));
    for (int i=0;i<SIZE;i++) {
      for (int j=0;j<SIZE;j++) {
        *A(i,j) = rand() / 10000;
      }
      b[i] = rand() / 10000;
    }

    //Start timer
    auto begin = std::chrono::high_resolution_clock::now();

    //Run guassian elimination
    GE(numSlaves);

    //End Timer, get duration
    auto end = std::chrono::high_resolution_clock::now();
    auto dur = chrono::duration_cast<chrono::milliseconds>(end-begin).count();


    //Tell all slaves to exit
    int exitCond = -1;
    for (int slave=1;slave<numSlaves;slave++) 
      MPI_Send(&exitCond, 1, MPI_INT, slave, host_to_slave, MPI_COMM_WORLD);

    //Print out matrix's and result
    /**print();**/
    verify();
    printf("ID:%d,%s,%d,%ld\n",id,TYPE,SIZE,dur);
  } 
  //Slave code
  else 
  {
    _TYPE_ *tempScale = (_TYPE_*) malloc(SIZE*sizeof(_TYPE_));
    _TYPE_ *tempRow = (_TYPE_*) malloc(rowsPerProc*SIZE*sizeof(_TYPE_));
    while (receiveChunk(id,*tempRow,*tempScale)) {} //Exit cond is -1
    free(tempScale);
    free(tempRow);
  }
  MPI_Finalize();

  return 0;
}

