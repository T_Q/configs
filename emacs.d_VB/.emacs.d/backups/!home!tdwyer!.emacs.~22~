;;Preliminary 
(setq user-full-name "Tyler Q. Dwyer"
      user-mail-address "tdwyer@ece.ubc.ca")

(fset 'yes-or-no-p 'y-or-n-p) ;;y/n instead of yes/no
(global-set-key [(control z)] nil) ;;Disable control-z exit
(global-linum-mode t)
(column-number-mode)

;;Minimap
;;(minimap-create)
;;(setq minimap-window-location 'right)

;;Autocomplete
;;(require 'auto-complete-mode)
;;(ac-config-default)

;;Git stuff
(global-set-key (kbd "C-x g") 'magit-status)



;;Window Size
(when window-system (set-frame-size (selected-frame) 150 45))

;;Window Navigation
(global-set-key (kbd "C-c <left>")  'windmove-left)
(global-set-key (kbd "C-c <right>") 'windmove-right)
(global-set-key (kbd "C-c <up>")    'windmove-up)
(global-set-key (kbd "C-c <down>")  'windmove-down)

;;Cursor Navigation
(add-to-list 'load-path "~/.emacs.d/elpa/ace-jump-mode-20140616.115/")
(require 'ace-jump-mode)
(define-key global-map (kbd "C-c SPC") 'ace-jump-mode)

;;Development
(setq c-default-style "linux"
      c-basic-offset 4)

;;Backups
(setq backup-directory-alist '(("." . "~/.emacs.d/backups")))
(setq delete-old-versions -1)
(setq version-control t)
(setq vc-make-backup-files t)
(setq auto-save-file-name-transforms '((".*" "~/.emacs.d/auto-save-list/" t)))

;;Packages
(require 'package) 
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/"))
(when (< emacs-major-version 24)
  (add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/")))
(package-initialize)

;;(require 'guide-key)
;;(setq guide-key/guide-key-sequence '("C-x r" "C-x 4"))
;;(setq guide-key/guide-key-sequence t)
;;(guide-key-mode 1) ; Enable guide-key-mode
;;(setq guide-key/idle-delay 1)

(add-to-list 'load-path "/usr/share/emacs24/site-lisp/emacs-goodies-el/color-theme.el")
(require 'color-theme)
(eval-after-load "color-theme"
  '(progn
     (color-theme-initialize)
     (color-theme-taming-mr-arneson)))

(defun duplicate-line()
  (interactive)
  (move-beginning-of-line 1)
  (kill-line)
  (yank)
  (open-line 1)
  (next-line 1)
  (yank)
  )
(global-set-key (kbd "C-d") 'duplicate-line)

(require 'helm)
(setq helm-idle-delay 0.0 ; update fast sources immediately (doesn't).
          helm-input-idle-delay 0.01  ; this actually updates things
                                        ; reeeelatively quickly.
          helm-yas-display-key-on-candidate t
          helm-quick-update t
          helm-M-x-requires-pattern nil
          helm-ff-skip-boring-files t)
(global-set-key (kbd "C-c h") 'helm-mini)
(global-set-key (kbd "C-x b") 'helm-buffers-list)
(global-set-key (kbd "M-x") 'helm-M-x)
(global-set-key (kbd "M-y") 'helm-show-kill-ring)
(global-set-key (kbd "C-x C-f") 'helm-find-files)

(require 'undo-tree)
(global-undo-tree-mode)
(global-set-key (kbd "C-x U") 'undo-tree-visualize)
(setq undo-tree-visualizer-timestamps t)
(setq undo-tree-visualizer-diff t)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(column-number-mode t)
 '(display-time-mode t)
 '(fringe-mode 0 nil (fringe))
 '(inhibit-startup-screen t)
 '(show-paren-mode t)
 '(tool-bar-mode nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )




(global-set-key (kbd "<f1>") 'reposition-window)
(global-set-key (kbd "<f9>") (lambda () (interactive) (insert (shell-command "send_host &" ))))
(global-set-key (kbd "<f10>") (lambda () (interactive) (insert (shell-command "run_host &" ))))
(global-set-key (kbd "<f5>") 'compile)
(global-set-key (kbd "TAB") 'tab-to-tab-stop)
(global-set-key (kbd "<backtab>") 'un-indent-by-removing-4-spaces)
(defun un-indent-by-removing-4-spaces ()
  "remove 4 spaces from beginning of of line"
  (interactive)
  (save-excursion
    (save-match-data
      (beginning-of-line)
      ;; get rid of tabs at beginning of line
      (when (looking-at "^\\s-+")
        (untabify (match-beginning 0) (match-end 0)))
      (when (looking-at "^    ")
        (replace-match "")))))


