 #include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "../lib/libfixmath/fixmath.h"
#include "../../../repository/lib/vbxapi/vbx.h"
#define SIZE 128

int fixed = 1;
int serial = 0;
int vbx = 0;
int main()
{

  //Create matrix's 
  float *GE_a = (float*) malloc(SIZE*SIZE*sizeof(float));
  float *GE_b = (float*) malloc(SIZE*sizeof(float));
  float *GE_x = (float*) malloc(SIZE*sizeof(float));

  //Initialize to random
  for (int i=0;i<SIZE;i++) {
    for (int j=0;j<SIZE;j++) {
	GE_a[i*SIZE+j] = rand() % 64;
    }
    GE_b[i] = rand() % 64;
  }

  //Convert to fixed 
  fix16_t *fix_a = (fix16_t*) malloc(SIZE*SIZE*sizeof(fix16_t));
  fix16_t *fix_b = (fix16_t*) malloc(SIZE*sizeof(fix16_t));
  fix16_t *fix_x = (fix16_t*) malloc(SIZE*sizeof(fix16_t));
  for (int i=0;i<SIZE;i++) {
      for (int j=0;j<SIZE;j++) {
	  fix_a[i*SIZE+j] = fix16_from_float(GE_a[i*SIZE+j]);
      }
      fix_b[i] = fix16_from_float(GE_b[i]);
  }

  //Reduce
  if (serial) {
      for (int curLoc = 0; curLoc < SIZE; curLoc++) {
	  for (int row = curLoc + 1; row < SIZE; row++) {
	      float scale = GE_a[row*SIZE+curLoc] / GE_a[curLoc*SIZE+curLoc];
	      GE_b[row] = GE_b[row] - (scale * GE_b[curLoc]);
      
	      for (int col = curLoc + 1; col < SIZE; col++) {
		  GE_a[row*SIZE+col] = GE_a[row*SIZE+col] - (scale * GE_a[curLoc*SIZE+col]);
	      }
	      GE_a[row*SIZE+curLoc] = 0;
	  }
      }
  }

//A small change

  
  if (fixed) {
      for (int curLoc = 0; curLoc < SIZE; curLoc++) {
	  for (int row = curLoc + 1; row < SIZE; row++) {
	      fix16_t scale = fix16_div(fix_a[row*SIZE+curLoc],fix_a[curLoc*SIZE+curLoc]);
	      fix_b[row] = fix_b[row] - fix16_mul(scale,fix_b[curLoc]);
      
	      for (int col = curLoc + 1; col < SIZE; col++) {
		  fix_a[row*SIZE+col] = fix_a[row*SIZE+col] - fix16_mul(scale,fix_a[curLoc*SIZE+col]);
	      }
	      fix_a[row*SIZE+curLoc] = 0;
	  }
      }
  }

#if VBX_SIMULATOR==1
  //initialize with 4 lanes,and 64kb of sp memory
  //word,half,byte fraction bits 16,15,4 respectively
  vbxsim_init( 4, 64, 256, 16, 15, 4 );
#endif

  //Allocate vectors in scratchpad
  vbx_word_t* a = vbx_sp_malloc( SIZE*sizeof(vbx_word_t) );
  vbx_word_t* vcurLoc = vbx_sp_malloc( SIZE*sizeof(vbx_word_t) );
  vbx_word_t* vscale = vbx_sp_malloc( SIZE*sizeof(vbx_word_t) );
  vbx_word_t* tmp = vbx_sp_malloc( SIZE*sizeof(vbx_word_t) );
   
  vbx_set_vl(SIZE);
  //Reduce
  if (vbx) {
      for (int curLoc = 0; curLoc < SIZE; curLoc++) {
	  vbx_dma_to_vector(vcurLoc,fix_a+(curLoc*SIZE),SIZE*sizeof(fix16_t));
	  for (int row = curLoc + 1; row < SIZE; row++) {

	      fix16_t fixed_scale = fix16_div(fix_a[row*SIZE+curLoc],fix_a[curLoc*SIZE+curLoc]);

	      vbx_dma_to_vector(a,fix_a+(row*SIZE),SIZE*sizeof(fix16_t));
	      vbx(SVW,VMOV,vscale,fixed_scale,0);
	      vbx(VVW,VMULFXP,tmp,vscale,vcurLoc);
	      vbx(VVW,VSUB,a,a,tmp);
	      vbx_dma_to_host(fix_a+(row*SIZE),a,SIZE*sizeof(fix16_t));
	      vbx_sync();
	      fix_a[row*SIZE+curLoc] = 0;
	      fix_b[row] = fix_b[row] - fix16_mul(fixed_scale,fix_b[curLoc]);
	  }
      }
  }

  vbx_sync();
  //Move out of fixed point
  if (!serial) {
      for (int i=0;i<SIZE;i++) {
	  for (int j=0;j<SIZE;j++) {
	      //float x = GE_a[i*SIZE+j];
	      //float y = fix16_to_float(fix_a[i*SIZE+j]);
	      GE_a[i*SIZE+j] = fix16_to_float(fix_a[i*SIZE+j]);
	      //printf("x = %f, y = %f \n",x,y);
	  }
	  //float x = GE_b[i];
	  //float y = fix16_to_float(fix_b[i]);
	  GE_b[i] = fix16_to_float(fix_b[i]);
	  //printf("B x = %f, y = %f \n",x,y);
      }
  }

  
  //print out simulator statistics
//  vbxsim_print_stats();

  //Solve

  if(serial) {
      //Solve Float
      for (int row = SIZE-1; row >= 0; row--) {
	  float tmp = GE_b[row];
	  for (int col = SIZE-1; col > row; col--)
	      tmp = tmp - (GE_x[col] * GE_a[row*SIZE+col]);
	  GE_x[row] = tmp / GE_a[row*SIZE+row];
      }
  } else {
      //Solve Fixed
      for (int row = SIZE-1; row >= 0; row--) {
	  fix16_t tmp = fix_b[row];
	  for (int col = SIZE-1; col > row; col--)
	      tmp = tmp - fix16_mul(fix_x[col],fix_a[row*SIZE+col]);
	  fix_x[row] = fix16_div(tmp,fix_a[row*SIZE+row]);
      }
  }
  //Check
  float aSum = 0; float bSum = 0; float xSum = 0;
  if (serial) {
      for (int row = 0; row < SIZE; row++) {
	  for (int col = 0; col < SIZE; col++)
	      aSum += GE_a[row*SIZE+col];
	  bSum += GE_b[row];
	  xSum += GE_x[row];
      }
  } else {

      for (int row = 0; row < SIZE; row++) {
	  for (int col = 0; col < SIZE; col++)
	      aSum += fix16_to_float(fix_a[row*SIZE+col]);
	  bSum += fix16_to_float(fix_b[row]);
	  xSum += fix16_to_float(fix_x[row]);
      }
  }
  printf("Update: aSum=%f, bSum=%f, xSum=%f\n",aSum,bSum,xSum);
  
	
  return 0;
}


/*



     

    
  vbx_dma_to_vector(vcurLoc,fix_a+(curLoc*SIZE),SIZE*sizeof(fix16_t)); //GE_a[curLoc]
  vbx_sync(); 

      

  for (int col=curLoc+1;col<curLoc+2;col++) {
  printf("Scaled: (%f,%f)\n",(float)vcurLoc[col]/65536*float_scale,(float)tmp[col]/65536);
  printf("Scale: (%f,%f)\n",float_scale,((float)vscale[col])/65536);
  //printf("(%f,%f),",fix_a[row*SIZE+col],((float)fix_a[row*SIZE+col])/65536);
  //	printf("(%f,%f),",GE_a[row*SIZE+col],((float)fix_a[row*SIZE+col])/65536);
  }
  printf("\n");



  printf("Scales for curLoc %d, row %d, float %f, fixed %f, true %f\n",curLoc, row,float_scale,(float)(fixed_scale)/65536,scale);
  for (int col = curLoc + 1; col < SIZE; col++) {
	
  GE_a[row*SIZE+col] = (scale * GE_a[curLoc*SIZE+col]) - GE_a[row*SIZE+col];
  }


     
  //Solve
  for (int row = SIZE-1; row >= 0; row--) {
    float tmp = b[row];
    for (int col = SIZE-1; col >= row; col--)
      tmp -= x[col] * a[row*SIZE+col];
    x[row] = tmp / a[row*SIZE+row];
  }

  //Check
  int aSum = 0; int bSum = 0; int xSum = 0;
  for (int row = 0; row < SIZE; row++) {
    for (int col = 0; col < SIZE; col++)
      aSum += a[row*SIZE+col];
    bSum += b[row];
    xSum += x[row];
  }
  printf("aSum=%d, bSum=%d, xSum=%d\n",aSum,bSum,xSum);
  */

