#include "CallGraph.h"

using namespace callgraphs;


RegisterPass<WeightedCallGraphPass> X("weightedcg",
    "construct a weighted call graph of a module");
char WeightedCallGraphPass::ID = 0;

Function *hostFun, *inputFunA, *inputFunB; //Our functions
ValueToValueMapTy VMap;

/********************************************************************/

void getDeps(Instruction *instr, Instruction *moveToLoc, BasicBlock *scope_block) 
{
//Gets the branch dependencies and moves them to the conditional
//C = conditional
//A = First operand of conditional
//B = (Possible) second operand of conditional
//WARNING: Not sure if this appropriate in all cases
//         Does not perform check on where it is pulling
//         the dependencies from
  if (instr->getParent() != scope_block) return;
  instr->moveBefore(moveToLoc);
  if (isa<LoadInst>(instr)) return;
  
  if (instr->getNumOperands() == 0) return;
   auto *A = dyn_cast<Instruction>(instr->getOperandUse(0));
  if (A)
    getDeps(A,instr,scope_block);

  if (instr->getNumOperands() == 1) return;
  auto *B = dyn_cast<Instruction>(instr->getOperandUse(1));
  if (B)
    getDeps(B,instr,scope_block);
}

Function *WeightedCallGraphPass::splitConditionals(Function &fun) 
{
  //Split Conditional BBs into and Unconditional and Conditional
  //Ac below, is a block with just the conditional in it
  //Before:  --> A0 --T> A1T
  //                --F> A1F
  //After:   --> A0 --> Ac --T> A1T
  //                       --F> A1F
  vector<BasicBlock*> conditionalBlocks;
  for (auto &bb : fun) {
    if (bb.getTerminator()->getNumSuccessors() == 2) {
      conditionalBlocks.push_back(&bb);
    }
  }
  for (auto &bb : conditionalBlocks) {
     BasicBlock *newB = SplitBlock(bb,bb->getTerminator(),this);
     auto *Br = newB->getTerminator();
     auto *C = dyn_cast<Instruction>(Br->getOperandUse(0));
     getDeps(C,Br,bb); 
  }
  return &fun;
}

void reorder_blocks(vector<bGroup*> bGroups,Function *f) 
{
  BasicBlock *tmp = &f->front();
  for (bGroup *g : bGroups) {
    if (tmp) 
      g->branch->moveAfter(tmp);
    else
      tmp = g->branch;
  
    if (g->joined) {
      g->converge->moveAfter(g->branch);
      g->diverge->moveAfter(g->converge);
      g->data_TT->moveAfter(g->diverge);
      g->data_FF->moveAfter(g->data_TT);
      g->data_TF->moveAfter(g->data_FF);
      g->data_FT->moveAfter(g->data_TF);
      tmp = g->data_FT;
    } else {
      g->data_F->moveAfter(g->branch);
      g->data_T->moveAfter(g->branch);
      tmp = g->data_F;
    }
  }
}

void make_into_return(BasicBlock *b) 
{
  //Remove unconditional from AB_A
  b->getInstList().pop_back(); 

  //Create return instruction
  Builder.SetInsertPoint(b);
  Builder.CreateRetVoid();

}

void fix_clone_use_tmp(BasicBlock* orig, BasicBlock* clone)
{
  //Go through each instruction in the original basic block
  for (Instruction &i : *orig) {
    vector<Instruction*> Users;
    vector<Use*> Use_x;
    
    //Go through each 'use' of this instruction (the output of i)
    //The 'user' is the instruction using the 'use'
    //Add things to vector to avoid clobbering
    for (auto UseI = i.use_begin(), UseE = i.use_end(); UseI != UseE; ++UseI) { 
      Use &U = *UseI;
      Instruction *User = dyn_cast<Instruction>(U.getUser());

      if (User->getParent() == clone) {
        Users.push_back(User);
        Use_x.push_back(&U);
      }
    }

    //Go back through and update uses with cloned use
    for (int i =0;i<(int)Users.size();i++) 
      Users[i]->replaceUsesOfWith(*Use_x[i],VMap[*Use_x[i]]);
  }
}


BasicBlock* combine_blocks(BasicBlock* A, BasicBlock* B, Function *f) 
{
  //Create clones of each block
  BasicBlock *AB_A = CloneBasicBlock(A,VMap,"",f);
  BasicBlock *AB_B = CloneBasicBlock(B,VMap,"",f);

  fix_clone_use_tmp(A,AB_A);
  fix_clone_use_tmp(B,AB_B);

  //Remove unconditional from AB_A
  AB_A->getInstList().pop_back(); 
//  AB_B->getInstList().pop_back(); 

  /*Builder.SetInsertPoint(AB_A);
  Instruction *i = Builder.CreateBr(AB_B);
  i->moveBefore(AB_A->getTerminator());
  AB_A->getInstList().pop_back();*/
 
  //Merge AB_A and AB_B
  AB_A->getInstList().splice(AB_A->end(),AB_B->getInstList());

  //Delete second block (should have no instructions in it)
  AB_B->eraseFromParent();;

  return AB_A;
}

BasicBlock * create_top_block(BasicBlock *A, BasicBlock *B, BasicBlock *con, BasicBlock *div, Function *f, Value *A_cond) 
{
  //Create clones of each block
  BasicBlock *A_clone = CloneBasicBlock(A,VMap,"",f);
  BasicBlock *B_clone = CloneBasicBlock(B,VMap,"",f);

  fix_clone_use_tmp(A,A_clone);
  fix_clone_use_tmp(B,B_clone);
 
  A_cond = A_clone->getTerminator()->getOperand(0);
  Value *B_cond = B_clone->getTerminator()->getOperand(0);
/*
 * Direct merge method
  //Remove terminator instructions from both A and B clones
  */
  A_clone->getInstList().pop_back(); 
  B_clone->getInstList().pop_back(); 

  //Merge A and B clones together
  A_clone->getInstList().splice(A_clone->end(),B_clone->getInstList());

  //Delete second block (should have no instructions in it)
  B_clone->eraseFromParent();

  Builder.SetInsertPoint(A_clone);
  Value *top_cond = Builder.CreateXor(A_cond, B_cond,"");
  Builder.CreateCondBr(top_cond,div,con);

/*
* Unconditional Branch connection method

  Builder.SetInsertPoint(A_clone);
  Instruction *iA = Builder.CreateBr(B_clone);
  iA->moveBefore(A_clone->getTerminator());
  
  Value *top_cond = Builder.CreateXor(A_cond, B_cond,"");
  Instruction *iB = Builder.CreateCondBr(top_cond,div,con);
  iB->moveBefore(B_clone->getTerminator());
  dyn_cast<Instruction>(top_cond)->moveBefore(iB);

  A_clone->getInstList().pop_back(); 
  B_clone->getInstList().pop_back(); 
*/
  return A_clone;
}

bGroup *combine_groups(bGroup *A, bGroup *B, LLVMContext &context, Function *f)
{
  if (A->joined_to[B])  return A->joined_to[B];

  Value *A_cond = A->branch->getTerminator()->getOperand(0);
  
  BasicBlock *top;
  BasicBlock *converge = BasicBlock::Create(context,"Converge",f);
  BasicBlock *diverge = BasicBlock::Create(context,"Diverge",f);
  if (A_cond == Builder.getTrue()) 
  {
    top = BasicBlock::Create(context,"Top",f);
    Builder.SetInsertPoint(top);
    Builder.CreateCondBr(Builder.getTrue(),converge,diverge);
  } else 
    top = create_top_block(A->branch,B->branch,converge,diverge,f,A_cond);

  // -- Create new combined/merged/joined group -- //
  bGroup* gAB = new bGroup(curID);
  gAB->joined = true;
  gAB->join_A = A;
  gAB->join_B = B;
  gAB->branch = top;
  gAB->top = top;
  gAB->converge = converge;
  gAB->diverge = diverge;
  A->joined_to[B] = gAB;
  B->joined_to[A] = gAB;
  top->setName("Top" + to_string(gAB->ID));
  converge->setName("Converge" + to_string(gAB->ID));
  diverge->setName("Diverge" + to_string(gAB->ID));

  // ---- Converge Blocks ---- //
  gAB->data_TT = combine_blocks(A->data_T,B->data_T,f);
  gAB->data_FF = combine_blocks(A->data_F,B->data_F,f);
  gAB->data_TT->setName("data" + to_string(gAB->ID) + "_TT");
  gAB->data_FF->setName("data" + to_string(gAB->ID) + "_FF");
  Builder.SetInsertPoint(converge);
  Builder.CreateCondBr(A_cond,gAB->data_TT,gAB->data_FF);
  fix_clone_use_tmp(A->branch,converge);
  // ---- Diverge Blocks ---- //
  gAB->data_TF = combine_blocks(A->data_T,B->data_F,f);
  gAB->data_FT = combine_blocks(A->data_F,B->data_T,f);
  gAB->data_TF->setName("data" + to_string(gAB->ID) + "_TF");
  gAB->data_FT->setName("data" + to_string(gAB->ID) + "_FT");
  Builder.SetInsertPoint(diverge);
  Builder.CreateCondBr(A_cond,gAB->data_TF,gAB->data_FT);
  fix_clone_use_tmp(A->branch,diverge);

  return gAB;
}

vector<bGroup*> connect_groups(vector<bGroup*> gABs, bool do_diverge, LLVMContext &context, Function *f)
{

  for (int i=0; i<(int)gABs.size(); i++) {
    bGroup *gAB = gABs[i];
    bGroup* A = gAB->join_A;
    bGroup* B = gAB->join_B;

    // -- True with True  -- //
    if (A->TReturns && B->TReturns)
      gAB->next_TT = NULL;
    else if (A->TReturns)
      gAB->next_TT = B->TNext;
    else if (B->TReturns)
      gAB->next_TT = A->TNext;
    else {
      gAB->next_TT = combine_groups(A->TNext,B->TNext,context,f);
      if (find(gABs.begin(),gABs.end(),gAB->next_TT) == gABs.end())
        gABs.push_back(gAB->next_TT);
    }

    if (gAB->next_TT)
      if (gAB->data_TT->getTerminator()->getNumSuccessors() == 0) {}
      else
        gAB->data_TT->getTerminator()->setSuccessor(0,gAB->next_TT->branch);
    else 
      make_into_return(gAB->data_TT);

    //  -- False with False -- //
    if (A->FReturns && B->FReturns)
      gAB->next_FF = NULL;
    else if (A->FReturns)
      gAB->next_FF = B->FNext;
    else if (B->FReturns)
      gAB->next_FF = A->FNext;
    else { 
      gAB->next_FF = combine_groups(A->FNext,B->FNext,context,f);
      if (find(gABs.begin(),gABs.end(),gAB->next_FF) == gABs.end())
        gABs.push_back(gAB->next_FF);
    }

    if (gAB->next_FF)
      if (gAB->data_FF->getTerminator()->getNumSuccessors() == 0) {}
      else
        gAB->data_FF->getTerminator()->setSuccessor(0,gAB->next_FF->branch);
    else
      make_into_return(gAB->data_FF);

    if (do_diverge) {  
      //  -- True with False  --  //
      if (A->TReturns && B->FReturns)
        gAB->next_TF = NULL;
      else if (A->TReturns)
        gAB->next_TF = B->FNext;
      else if (B->FReturns)
        gAB->next_TF = A->TNext;
      else {
        gAB->next_TF = combine_groups(A->TNext,B->FNext,context,f);
        if (find(gABs.begin(),gABs.end(),gAB->next_TF) == gABs.end())
          gABs.push_back(gAB->next_TF);
      }

      if (gAB->next_TF) {
        if (gAB->data_TF->getTerminator()->getNumSuccessors() == 0) {}
        else
          gAB->data_TF->getTerminator()->setSuccessor(0,gAB->next_TF->branch);
      }
      else 
        make_into_return(gAB->data_TF);

      //  -- False with True  --  //
      if (A->FReturns && B->TReturns)
        gAB->next_FT = NULL;
      else if (A->FReturns)
        gAB->next_FT = B->TNext;
      else if (B->TReturns)
        gAB->next_FT = A->FNext;
      else {
        gAB->next_FT = combine_groups(A->FNext,B->TNext,context,f);
        if (find(gABs.begin(),gABs.end(),gAB->next_FT) == gABs.end())
          gABs.push_back(gAB->next_FT);
      }

      if (gAB->next_FT) {
        if (gAB->data_FT->getTerminator()->getNumSuccessors() == 0) {}
        //        outs() << "No Successors on .. \n" << *gAB->data_FT << "\n";
        else
          gAB->data_FT->getTerminator()->setSuccessor(0,gAB->next_FT->branch);
      } else
        make_into_return(gAB->data_FT);
    }
  }
  return gABs;
}

vector<bGroup*> interweave(Function *f, vector<bGroup*> A, vector<bGroup*> B) 
{
  LLVMContext &context = f->getContext();
  vector<bGroup*> AB;

  //Create combined conditional groups
  for (int i=0; i<(int)A.size(); i++) {
    bGroup *gAB = combine_groups(A[i],B[i],context,f);
    AB.push_back(gAB);
  }

  //Connect conditional groups together (do not do divergent groups) 
  AB = connect_groups(AB,true,context,f);
  

  return AB;
}

Function *create_interweave_function(Function *f_A, Function *f_B)
{
//  Function *f = CloneFunction(f_A,VMap,false);
//  f->setName("Interweave");

  vector<Type*> ArgTypes;
  for (Argument &I : f_A->args())
   if (VMap.count(&I) == 0)
    ArgTypes.push_back(I.getType()); 

  for (Argument &I : f_B->args())
   if (VMap.count(&I) == 0)
    ArgTypes.push_back(I.getType()); 

  FunctionType *FTy = FunctionType::get(f_A->getFunctionType()->getReturnType(), 
      ArgTypes, f_A->getFunctionType()->isVarArg());

  Function *f = Function::Create(FTy, f_A->getLinkage(), "Interweave");

  Function::arg_iterator DestI = f->arg_begin();
  for (Argument &I : f_A->args())
    if (VMap.count(&I) == 0) {
      DestI->setName(I.getName());
      VMap[&I] = &*DestI++;
    }
  for (Argument &I : f_B->args())
    if (VMap.count(&I) == 0) {
      DestI->setName(I.getName());
      VMap[&I] = &*DestI++;
    }

  SmallVector<ReturnInst*,8> Returns;
  CloneFunctionInto(f,f_A,VMap,false,Returns,"_A");
  CloneFunctionInto(f,f_B,VMap,false,Returns,"_B");

  return f;
}

void prepFun(Function *f) 
{
  BasicBlock *Entry = BasicBlock::Create(f->getContext(),"Entry", f);
  BasicBlock *old_entry = &f->front();
  old_entry->setName("old_entry");
  Entry->moveBefore(&f->front());
  Builder.SetInsertPoint(Entry);
  Builder.CreateCondBr(Builder.getTrue(),old_entry,old_entry);
}

bool WeightedCallGraphPass::runOnModule(Module &m) 
{

  //Get all the functions we are interested in
  for (Function &fun : m){
    if (fun.isDeclaration()) continue;
    if (fun.getName().find("main") != string::npos) 
      hostFun = &fun;
    if (fun.getName().find("xxxAxxx") != string::npos) 
      inputFunA = &fun;
    if (fun.getName().find("xxxBxxx") != string::npos) 
      inputFunB = &fun;
    if (fun.getName().find("LBM_performStreamCollide") != string::npos) 
      inputFunA = &fun;
  } 

  //Split up branch statements 
  splitConditionals(*inputFunA);
  splitConditionals(*inputFunB);

  //Create combined function and load cloned blocks into it
  Function *f = create_interweave_function(inputFunA,inputFunB);
  m.getFunctionList().push_back(f);

  //Get a list of blocks from function A
  vector<BasicBlock*> funA_blks;  
  for (auto &b : *inputFunA)
    funA_blks.push_back(dyn_cast<BasicBlock>(VMap[&b]));

  //Get a list of blocks from function B
  vector<BasicBlock*> funB_blks;  
  for (auto &b : *inputFunB) 
    funB_blks.push_back(dyn_cast<BasicBlock>(VMap[&b]));

  //Convert functions into conditional form
  vector<bGroup*> bGroups_A = reBuild_Function(inputFunA,f,funA_blks,true,VMap); 
  vector<bGroup*> bGroups_B = reBuild_Function(inputFunB,f,funB_blks,true,VMap); 

  //Interweave the two functions together in the interweaving function
  vector<bGroup*> bGroups_AB = interweave(f,bGroups_A,bGroups_B);

  //Get all conditional blocks: from funA, funB, and interweaved
  vector<bGroup*> all_groups;
  all_groups.insert(all_groups.end(),bGroups_AB.begin(),bGroups_AB.end());
  all_groups.insert(all_groups.end(),bGroups_A.begin(),bGroups_A.end());
  all_groups.insert(all_groups.end(),bGroups_B.begin(),bGroups_B.end());

  //Put them in a nice, readable order
  reorder_blocks(all_groups,f);

  //Get the entry points, order them, and connect appropriately
  BasicBlock *A_front = dyn_cast<BasicBlock>(VMap[&inputFunA->front()]);
  BasicBlock *B_front = dyn_cast<BasicBlock>(VMap[&inputFunB->front()]);
  A_front->moveBefore(&f->front());
  B_front->moveAfter(&f->front());
  if (A_front->getTerminator()->getNumSuccessors() > 0)
    A_front->getTerminator()->setSuccessor(0,B_front);
  else {
    A_front->getInstList().pop_back(); 
    Builder.SetInsertPoint(A_front);
    Builder.CreateBr(B_front);
  }
  if (B_front->getTerminator()->getNumSuccessors() > 0)
    B_front->getTerminator()->setSuccessor(0,all_groups[0]->branch);
  else {}

  //Insert interweaved function into main method
  Value * A_op, *B_op;
  Instruction *C_loc;
  for (auto &b : *hostFun) {
    for (auto &i : b) {
      CallSite cs(&i);
      if (!cs.getInstruction()) continue;

      Value *called = cs.getCalledValue()->stripPointerCasts();
      if (dyn_cast<Function>(called)->getName().find("xxxAxxx") != string::npos) 
        A_op = i.getOperand(0);

      if (dyn_cast<Function>(called)->getName().find("xxxBxxx") != string::npos)
        B_op = i.getOperand(0);

      if (dyn_cast<Function>(called)->getName().find("xxxCxxx") != string::npos) {
        C_loc = &i;
      }

    } 
  }
  Instruction* Interweave_call = Builder.CreateCall2(f,A_op,B_op);
  Interweave_call->moveBefore(C_loc);
  
//  outs() << m;
/*  outs() << "digraph {\n compound=true node [shape=oval];\n";
  print_dots(all_groups,"Interweaved");
//  print_dots(bGroups_A,"Single Function");
//  print_dots(bGroups_B,"Function B");
  outs() << "}\n";*/
  return true;
}

void WeightedCallGraphPass::print(raw_ostream &out, const Module *m) const {} 
//void interweave(vector<cBlock*> A, vector<cBlock*> B) {} 

