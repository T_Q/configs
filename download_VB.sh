#Description: Download config files from git repository, then copy/rename them as necessary
#Creation date: October 24, 2015
#Created by: Tyler Dwyer

#Move to config directory
cd ~/configs/

#Pull all updated config files
git pull --all

#Copy over and rename appropriately
cp -v .vimrc .emacs .bash_aliases .tmux.conf ~/
#cp -v .bashrc_wily ~/.bashrc
#cp -v .startup_wily ~/.startup
