#Description: Copy files into git repository, and upload to gitbucket
#Creation date: October 24, 2015
#Created by: Tyler Dwyer

#Copy and rename all config files into git repo
cd ~/
cp -v .vimrc .emacs .bash_aliases .tmux.conf ~/configs/
cp -vr ~/.emacs.d/ ~/configs/emacs.d_VB
cp -v .bashrc ~/configs/.bashrc_VB
cp -v .startup ~/configs/.startup_VB

#Update git repo
cd ~/configs/
git add .
git commit -m "`date`" 
git push origin master
